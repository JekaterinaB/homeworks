import java.util.Scanner;

public class SquareMeters
{
    public static void main (String [] args)
    {
        double squareM, squareKm;

        Scanner input = new Scanner(System.in);
        System.out.println("Enter the value in square meters: ");
        squareM=input.nextInt();

        squareKm = squareM * Math.pow(10, -6);
        System.out.println("In square km: "+squareKm);
    }
}