import java.util.Scanner;

public class Currency
{
    public static double convert(double amount)
    {
        double eur = amount / 1.21204; //rate of 6th of December
        return eur;
    }
    public static void main(String[] args)
    {
        Scanner input = new Scanner(System.in);
        System.out.println("Enter amount in dollars: ");
        double amount = input.nextDouble();
        System.out.println("Amount in eur: ");
        System.out.print(convert(amount));
        input.close();
    }
}