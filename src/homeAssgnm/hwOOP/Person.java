package assignmentOOP;

public class Person {

    protected String name, surname;

        protected String getName() {
                return name;
        }

        public void setName(String name) {
            this.name = name;
            }

        public String getSurname() {
            return surname;
        }

        public void setSurname(String surname) {
        this.surname = surname;
        }

        public Person() {
}

        public Person(String name, String surname) {
            this.name = name;
            this.surname = surname;
        	}

        	@Override
            public String toString() {
                    return "Name: " + name + System.lineSeparator()+ "Surname: " + surname+ System.lineSeparator() ;
        	}

}