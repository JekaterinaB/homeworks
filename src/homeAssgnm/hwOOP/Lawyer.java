package homeAssgnm.hwOOP;

public class Lawyer extends Person {

        private int lawyerID, helpedInCrimesSolving;

        public Lawyer() {
        }

        public Lawyer(String name, String surname, int lawyerID, int helpedInCrimesSolving) {
                super(name, surname);

                this.lawyerID = lawyerID;
                this.helpedInCrimesSolving = helpedInCrimesSolving;
        }

        public int getLawyerID() {
                return lawyerID;
        }

        public int getHelpedInCrimesSolving() {
                return helpedInCrimesSolving;
        }

        public void setHelpedInCrimesSolving(int helpedInCrimesSolving) {
                this.helpedInCrimesSolving = helpedInCrimesSolving;
        }

        public void setLawyerID(int lawyerID) {
                this.lawyerID = lawyerID;
        }

        @Override
        public String toString() {
                return super.toString() + "Lawyer ID: " + this.lawyerID + System.lineSeparator() + "Helped in crimes solving: "
                                + this.helpedInCrimesSolving;
        }

}
