package homeAssgnm.hwOOP;

import java.util.ArrayList;
import java.util.List;

        public class Execution {
            public static void main(String[] args) {

                officer[] officer = new officer[7];

                officer[0] = new officer("John", "Brown", "Los Angeles", 233, 12);
                officer[1] = new officer("Peter", "Parker", "New-York", 6765, 19);
                officer[2] = new officer("Slim", "Shady", "Los Angeles", 4565, 32);
                officer[3] = new officer("Ilon", "Blue", "New-York", 172, 24);
                officer[4] = new officer("Josh", "Black", "Los Angeles", 9812, 17);
                officer[5] = new officer("Nick", "White", "New-York", 169, 11);
                officer[6] = new officer("Ann", "Power", "Los Angeles", 902, 9);

                Lawyer lawyer1 = new Lawyer("Sarah", "Parker", 1234, 12);
                Lawyer lawyer2 = new Lawyer("Mike", "Winston", 2134, 20);
                Lawyer lawyer3 = new Lawyer("Peteris", "Berzins", 3434, 32);

                District district1 = new District("District27", "Los Angeles", 27);
                District district2 = new District("District13", "New-York", 13);

                for (int i = 0; i < 3; i++)
                    district1.addNewOfficer(officer[i]);

                for (int i = 3; i < 7; i++)
                    district2.addNewOfficer(officer[i]);

                System.out.println("The 1st district: " + System.lineSeparator() + district1);
                System.out.print(System.lineSeparator());
                System.out.println("The 2nd district: " + System.lineSeparator() + district2);
                System.out.print(System.lineSeparator());

                System.out.println("The 1st lawyer: " + System.lineSeparator() + lawyer1.toString());
                System.out.print(System.lineSeparator());
                System.out.println("The 2nd lawyer: " + System.lineSeparator() + lawyer2.toString());
                System.out.print(System.lineSeparator());
                System.out.println("The 3rd lawyer: " + System.lineSeparator() + lawyer3.toString());
                System.out.print(System.lineSeparator());

                List<Lawyer> departmentLawyers = new ArrayList<Lawyer>();
                departmentLawyers.add(lawyer1);
                departmentLawyers.add(lawyer2);
                departmentLawyers.add(lawyer3);

                int crimesTotalSum = 0;
                for (int i = 0; i < departmentLawyers.size(); i++) {
                    crimesTotalSum += departmentLawyers.get(i).getHelpedInCrimesSolving();
                }

                System.out.println("The number of crimes Lawyers solved: " + crimesTotalSum);
                System.out.print(System.lineSeparator());

                int bestResult = 0;
                int lawyerId = 0;
                for (int i = 0; i < departmentLawyers.size(); i++) {
                    if (departmentLawyers.get(i).getHelpedInCrimesSolving() > bestResult) {
                        bestResult = departmentLawyers.get(i).getHelpedInCrimesSolving();
                        lawyerId = i;
                    }
                }

                System.out.println("The most effective lawyer is " + System.lineSeparator()
                        + departmentLawyers.get(lawyerId));
            }
        }
