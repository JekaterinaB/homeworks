package homeAssgnm.hwOOP;

public class officer extends Person {

    private int officerID, crimesSolved;

    public officer() {
    }

    public officer(String name, String surname, String workingDistrict, int officerID, int crimesSolved) {
        super(name, surname);
        this.crimesSolved = crimesSolved;
        this.officerID = officerID;
    }

    public int getOfficerID() {
        return officerID;
    }

    public void setOfficerID(int officerID) {
        this.officerID = officerID;
    }

    public int getCrimesSolved() {
        return crimesSolved;
    }

    public void setCrimesSolved(int crimesSolved) {
        this.crimesSolved = crimesSolved;
    }

    @Override
    public String toString() {
        return super.toString() + "Officer ID: " + this.officerID + System.lineSeparator() + System.lineSeparator()
                + "Crimes Solved: " + this.crimesSolved;
    }

    int calculateLevel() {
        if (this.crimesSolved < 20)
            return 1;
        else if (this.crimesSolved >= 20 && this.crimesSolved < 40)
            return 2;
        else
            return 3;
    }

}