package homeAssgnm;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Execution {

    public static void main(String[] args) {

        Officer officer1 = new Officer("John", "Brown", 233, 12);
        Officer officer2 = new Officer("Peter", "Parker", 6765, 19);
        Officer officer3 = new Officer("Slim", "Shady", 4565, 32);
        Officer officer4 = new Officer("Ilon", "Blue", 172, 24);
        Officer officer5 = new Officer("Josh", "Black", 9812, 17);
        Officer officer6 = new Officer("Nick", "White", 169, 11);
        Officer officer7 = new Officer("Ann", "Power", 902, 9);

        Lawyer lawyer1 = new Lawyer("Sarah", "Parker", 1234, 12);
        Lawyer lawyer2 = new Lawyer("Mike", "Winston", 2134, 20);
        Lawyer lawyer3 = new Lawyer("Peteris", "Berzins", 3434, 32);

        District district1 = new District("District27", "Los Angeles", 27);
        District district2 = new District("District13", "New-York", 13);

        Officer[] officer = new Officer[7];

        for (int i = 0; i < officer.length; i++)
            officer[i] = new Officer();

        System.out.println("1st officer: ");
        System.out.println(officer[0] = officer1);
        System.out.println("2nd officer: ");
        System.out.println(officer[1] = officer2);
        System.out.println("3rd officer: ");
        System.out.println(officer[2] = officer3);
        System.out.println("4th officer: ");
        System.out.println(officer[3] = officer4);
        System.out.println("5th officer: ");
        System.out.println(officer[4] = officer5);
        System.out.println("6th officer: ");
        System.out.println(officer[5] = officer6);
        System.out.println("7th officer: ");
        System.out.println(officer[6] = officer7);

        for (int i = 0; i < 3; i++)
            district1.addNewOfficer(officer[i]);

        for (int i = 3; i < 7; i++)
            district2.addNewOfficer(officer[i]);

        System.out.println(district1);
        System.out.println(System.lineSeparator());
        System.out.println(district2);
        System.out.println(System.lineSeparator());

        System.out.println("1st lawyer: ");
        System.out.println(lawyer1);
        System.out.println("2nd lawyer: ");
        System.out.println(lawyer2);
        System.out.println("3rd lawyer: ");
        System.out.println(lawyer3);

        ArrayList<Lawyer> lawyers = new ArrayList<>();
        lawyers.add(lawyer1);
        lawyers.add(lawyer2);
        lawyers.add(lawyer3);

        int crimesSum = 0;
        for (Lawyer lawyer : lawyers) {
            crimesSum += lawyer.getHelpedInCrimesSolving();
        }
        System.out.println("Total number of crimes is " + crimesSum);

        Lawyer bestLawyer = null;
        for (Lawyer currentLawyer : lawyers) {
            if (bestLawyer == null || bestLawyer.getHelpedInCrimesSolving() < currentLawyer.getHelpedInCrimesSolving())
                bestLawyer = currentLawyer;
        }
        System.out.println(bestLawyer.getName() + " is the most effective Lawyer");
    }

}