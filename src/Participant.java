import java.util.Scanner;

public class Participant {

    public static String yourName;
    public static String yourSurname;
    public static int age;

    public static void main(String[] args) {

        Scanner myScanner = new Scanner(System.in);

        System.out.println("Enter your name");
        Participant.yourName = myScanner.nextLine();

        System.out.println("Enter your surname");
        Participant.yourSurname = myScanner.nextLine();

        System.out.println("Enter your age");
        Participant.age = Integer.parseInt(myScanner.nextLine());

        myScanner.close();
        Participant.show();
    }

    public static void show() {
        System.out.println("Participant's name: " + Participant.yourName);
        System.out.println("Participant's surname: " + Participant.yourSurname);
        System.out.println("Age: " + Participant.age);
    }

}