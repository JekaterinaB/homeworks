import java.util.Scanner;

public class Weight
{
    public static void main (String [] args)
    {
        double kg, pound;

        Scanner input = new Scanner(System.in);
        System.out.println("Enter the weight in kilograms: ");
        kg=input.nextInt();

        pound = kg * 2.205;
        System.out.println("In pounds: "+pound);
    }
}