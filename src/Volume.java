import java.util.Scanner;

public class Volume
{
    public static void main (String [] args)
    {
        double cubicM, litre;

        Scanner input = new Scanner(System.in);
        System.out.println("Enter the volume in cubic meters: ");
        cubicM=input.nextInt();

        litre = cubicM * 1000;
        System.out.println("In litres: "+litre);
    }
}