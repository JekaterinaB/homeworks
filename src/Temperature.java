import java.util.Scanner;

public class Temperature
{
    public static void main (String [] args)
    {
        float celsius, fahrenheit;

        Scanner input = new Scanner(System.in);
        System.out.println("Enter the temperature in celsius: ");
        celsius=input.nextInt();

        fahrenheit = celsius * 9/5 + 32;
        System.out.println("In fahrenheits: "+fahrenheit);
    }
}