import java.util.Scanner;

public class Length
{
    public static double convert(double amount)
    {
        double meter = amount / 0.3048;
        return meter;
    }
    public static void main(String[] args)
    {
        Scanner input = new Scanner(System.in);
        System.out.println("Enter length in meters: ");
        double amount = input.nextDouble();
        System.out.println("In feets: ");
        System.out.print(convert(amount));
        input.close();
    }
}