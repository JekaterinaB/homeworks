package Branching;

import java.util.Scanner;

public class DayOfTheWeek {

    public static void main(String[] args) {
        int day;
        Scanner myScanner = new Scanner(System.in);

        System.out.println("Please enter the number of day: ");
        day = Integer.parseInt(myScanner.nextLine());

        switch (day) {
            case 1:
                System.out.println("It is WORKING day Monday");
                break;
            case 2:
                System.out.println("It is WORKING day Tuesday");
                break;
            case 3:
                System.out.println("It is WORKING day Wednesday");
                break;
            case 4:
                System.out.println("It is WORKING day Thursday");
                break;
            case 5:
                System.out.println("It is last working day, Friday!");
                break;
            case 6:
            case 7:
                System.out.println("It is Weekend!");
                break;
            default:
                System.out.println("Day is incorrect, should be 1-7");
                return;
        }

    }

}