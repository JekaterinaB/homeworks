package Branching;

import java.util.Scanner;

public class Calculations {
    public static void main(String[] args) {

        Scanner myScanner = new Scanner(System.in);
        System.out.println("Please enter the first number:");
        int number1 = Integer.parseInt(myScanner.nextLine());

        System.out.println("Please enter the second number:");
        int number2 = Integer.parseInt(myScanner.nextLine());

        System.out.println("Enter operator: + or - or / or * or % or p or b or s");
        char operator = myScanner.nextLine().charAt(0);

        int result;

        switch (operator) {
            case '+':
                result = number1 + number2;
                break;
            case '-':
                result = number1 - number2;
                break;
            case '/':
                result = number1 / number2;
                break;
            case '*':
                result = number1 * number2;
                break;
            case '%':
                result = number1 % number2;
                break;
                //when trying to implement this:
            // case 'b':
            //                if (number1 > number2) {
            //                    System.out.println(number1 + " is bigger than " + number2);
            //                } else if (number1 < number2) {
            //                    System.out.println(number1 + " is smaller than " + number2);
            //                } else
            //                    System.out.println("The numbers are equal");
            //                break;
            // Appeared the error: java: variable result might not have been initialized

            default:
                System.out.println("You have entered wrong operator, please use + or - or / or * or % or p or b or s");
                myScanner.close();
                return;
        }
        System.out.println("Result is " + result);
        myScanner.close();
    }
}