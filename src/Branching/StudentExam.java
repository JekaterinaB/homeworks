package Branching;

import java.util.Scanner;

public class StudentExam {

    public static void main(String[] args) {
        Scanner myScanner = new Scanner(System.in);
        System.out.println("Enter your grade, please");
        char grade = myScanner.nextLine().charAt(0);

        switch (grade) {
            case 'A':
            case 'B':
                System.out.println("Perfect! You are so clever!");
                break;
            case 'C':
                System.out.println("Good! But you can do better!");
                break;
            case 'D':
            case 'E':
                System.out.println("It is not good! You should study!");
                break;
            case 'F':
                System.out.println("Fail! You need to retake the exam!");
                break;
            default:
                System.out.println("Invalid grade, please indicate your grade as A/B/C/D/E/F");
                return;
        }
    }
}