import java.util.Scanner;

public class HoursToDays
{
    public static void main (String [] args)
    {
        int hour, day;

        Scanner input = new Scanner(System.in);
        System.out.println("Enter the time in hours: ");
        hour=input.nextInt();

        day = hour / 24;
        System.out.println("In days: "+day);
    }
}