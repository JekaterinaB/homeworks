import java.util.Scanner;

public class Temperature2
{
    public static void main (String [] args)
    {
        double celsius, kelvin;

        Scanner input = new Scanner(System.in);
        System.out.println("Enter the temperature in celsius: ");
        celsius=input.nextInt();

        kelvin = celsius + 273.15;
        System.out.println("In kelvins: "+kelvin);
    }
}